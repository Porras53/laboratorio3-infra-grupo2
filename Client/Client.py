import socket
import os
import threading
from Utils import diagestHash
from Utils import write_log_file
import time
from datetime import datetime
import pyshark


SEPARATOR = "<SEPARATOR>"
BUFFER_SIZE = 4096
HOST = '192.168.0.23'
PORT = 9879

contador = 0

def correr_clientes(ClientSocket):  
    global contador 
    contador+=1
    capture = pyshark.LiveCapture(interface='Ethernet')
    capture.sniff(timeout=1)

    start_time = datetime.now()
    try:
        ClientSocket.connect((HOST, PORT))
        mensaje = "listo"
        ClientSocket.send(str.encode(mensaje))
        received = ClientSocket.recv(BUFFER_SIZE).decode()
        filename, filesize = received.split(SEPARATOR)
        # remove absolute path if there is
        filename = os.path.basename(filename)
        # convert to integer
        filesize = int(filesize)
        with open('./Client/ArchivosRecibidos/'+filename, "wb") as f:
            while True:
                # read 1024 bytes from the socket (receive)
                bytes_read = ClientSocket.recv(BUFFER_SIZE)
                #("fin".encode() in bytes_reÑad) or
                #print(bytes_read)
                #("fin" in bytes_read.decode()) or ((bytes_read) is None )or 
                if  ("fin" in bytes_read.decode()) or ((bytes_read) is None )or (not bytes_read):  
                    # nothing is received
                    # file transmitting is done
                    sin_fin=(bytes_read)[:-3]
                    #print(sin_fin)
                    f.write(sin_fin)
                    break
                # write to the file the bytes we just received
                f.write(bytes_read)
                # update the progress bar
        hashRecibido = ClientSocket.recv(BUFFER_SIZE)
        archivo = open('./Client/ArchivosRecibidos/'+filename, "rb")
        hashCalculado = diagestHash(archivo)
        archivo.close()

        integridad = "La integridad del archivo no se ve afectada en la transferencia" if (hashRecibido == hashCalculado) else "La integridad se vió afectada"
        ClientSocket.send(integridad.encode())

        confirmacion = ClientSocket.recv(BUFFER_SIZE).decode()

        print(confirmacion)
        finish_time = datetime.now()
        numeropack=str(capture).replace("<LiveCapture (","").replace(" packets)>","")
        write_log_file(filename, filesize, contador, confirmacion, start_time, finish_time, int(numeropack), BUFFER_SIZE)

        ClientSocket.close()
    except socket.error as e:
        print(str(e))
        ClientSocket.close()
    

numClientes = int(input("Ingrese el número de clientes:"))

ThreadCount = 0
while ThreadCount < numClientes:
    Client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    threading.Thread(target=correr_clientes, args=(Client, )).start()
    time.sleep(1)
    ThreadCount+=1   
