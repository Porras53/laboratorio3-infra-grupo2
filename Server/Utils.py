# importar librerias
import os
import hashlib
import datetime
import pyshark
import asyncio

# Funcion para imprimir los archivos diponibles en el servidor
def print_files():
    files_server = os.listdir('./files/')
    print("Los archivos que estan disponibles en el servidor son:")
    for i in range (len(files_server)):
        print(str(i+1) + "." + files_server[i])

# Funcion para obtener el archivo dado el numero ingresado como parametro
def get_file (num_file):
    #files_server = os.listdir('./files/')
    archivo = ''
    if num_file == "1":
        archivo = './files/prueba100MB.txt'
    elif num_file == "2":
        archivo = './files/prueba250MB.txt'

    return archivo
    # return files_server[int(num_file)-1]

# Funcion para generar el archivo de log en el directorio llamado Logs
def write_log_file (file_name, file_size, num_client, status_file, start_time, finish_time):
    today = datetime.datetime.now()
    name_file = str(today.year) + "-" + str(today.month) + "-" + str(today.day) + "-" + str(today.hour) + "-" + str(today.minute) + "-" + str(today.second) + "-log.txt"
    log_file = open("./Logs/" + name_file, "w")
    log_file.write("Nombre del archivo: " + file_name.split("/")[2] + "\n")
    log_file.write("Tamanio del archivo: " + str(file_size) + "\n")
    log_file.write("Cliente que realiza la transeferencia de archivos: " + str(num_client) + "\n")
    log_file.write("Estado de entrega del archivo: " + str(status_file) + "\n")
    log_file.write("Tiempo de transferencia: " + str(finish_time - start_time) + "\n")

# Funcion para generar el valor de hash del archivo trasmitido
def diagestHash (file):
    m = hashlib.sha512()
    m.update(file.read())
    return bytes(m.hexdigest(), 'utf-8')

packet_list = []


def process_packets(packet):
    global packet_list
    try:
        packet_version = packet.layers[1].version
        layer_name = packet.layers[2].layer_name
        packet_list.append(f'{packet_version}, {layer_name}, {packet.length}')
    except AttributeError:
        pass


def capture_packets(timeout):
    capture = pyshark.LiveCapture(interface='ens33')
    try:
      capture.apply_on_packets(process_packets, timeout=timeout)
    except asyncio.TimeoutError:
        pass
    finally:
        global packet_list
        return len(packet_list)