from Utils import *
import socket
import os
import threading
import time
from datetime import datetime
import pyshark


SEPARATOR = "<SEPARATOR>"
BUFFER_SIZE = 4096

ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '0.0.0.0'
port = 9879
try:
    ServerSocket.bind((host, port))
except socket.error as e:
    print(str(e))

NUM_CLIENTES = int(input("Ingrese el numero de clientes a conectar: "))

# Seleccionar el archivo que quiere enviar a los clientes
print_files()
numberFile = input("Escoge el archivo a enviar (1 o 2):")
FILE_NAME = None
n = False
FILE_NAME = get_file(numberFile)

print('Esperando las conexiones de los clientes...')
ServerSocket.listen(5)

contador = 0
FILE_SIZE = os.path.getsize(FILE_NAME)

def threaded_client(connection):

    start_time = datetime.now()
    with connection as c:
        global contador 
        data = c.recv(BUFFER_SIZE)
            
        if data.decode('utf-8')=="listo":
            contador+=1
            numero = contador 
            while contador < NUM_CLIENTES:   
                continue 
            extension = FILE_NAME.split('.')[2]
            c.send(f"Cliente{numero}-Prueba-{NUM_CLIENTES}.{extension}{SEPARATOR}{FILE_SIZE}".encode())

            with open(FILE_NAME, "rb") as f:
                while True:
                    # read the bytes from the file
                    bytes_read = f.read(BUFFER_SIZE)

                    if not bytes_read:
                        c.sendall("fin".encode())
                        #c.send(None)
                        break

                    c.sendall(bytes_read) 
          
            
            archivo = open(FILE_NAME, "rb")
            time.sleep(1)
            c.send(diagestHash(archivo))
            archivo.close()
            confirmacion = c.recv(BUFFER_SIZE).decode('utf-8')


            confirmacion_log = ""

            if confirmacion == "La integridad del archivo no se ve afectada en la transferencia":
                confirmacion_log= f"Thread-{numero}: El archivo ha sido enviado de manera correcta"
                c.send(f"Thread-{numero}: El archivo ha sido enviado de manera correcta".encode())
            else:
                confirmacion_log= f'Thread-{numero}: El archivo fue enviado de manera incorrecta'
                c.send(f"Thread-{numero}: El archivo fue enviado de manera incorrecta".encode())
            

            finish_time = datetime.now()
            write_log_file(FILE_NAME,FILE_SIZE,numero,confirmacion_log,start_time,finish_time)
  

ThreadCount = 0
while ThreadCount < NUM_CLIENTES:
    Client, address = ServerSocket.accept()
    print('Conectado con: ' + address[0] + ':' + str(address[1]))
    threading.Thread(target=threaded_client, args=(Client, )).start()
    ThreadCount+=1
    print('Numero del Thread: ' + str(ThreadCount))

ServerSocket.close()
